#!/bin/bash

./manage.py sqlflush | ./manage.py dbshell
./manage.py loaddata ./resources/testdata.json

python3 manage.py makemigrations
python3 manage.py migrate

python3 manage.py runserver