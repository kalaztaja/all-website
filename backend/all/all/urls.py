from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from allbackend import views
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = routers.DefaultRouter()


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view()),
    path('api/token/refresh', TokenRefreshView.as_view()),
    path('user/', views.CurrentUserView.as_view()),
    path('players/', views.PlayerViewSet.as_view(), name='players'),
    path('teams/', views.TeamViewSet.as_view(), name='teams'),
    path('users/', views.UsersViewSet.as_view(), name='users'),
    path('groups/', views.GroupViewSet.as_view(), name='groups')
]
