from django.contrib.auth.models import Group
from rest_framework import serializers
from .models import Team, Player, User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'uuid',
            'username',
            'password',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_active',
        )
        read_only_fields = ('is_staff',)

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.is_staff = False
        user.save()

        player = Player.objects.create(
            user=user
        )

        return user


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    players = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Team
        fields = ['name', 'tag', 'created', 'players']


class PlayerSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Player
        fields = ['user', 'nickname', 'team', 'created']
