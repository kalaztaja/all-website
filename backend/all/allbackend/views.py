from django.contrib.auth.models import Group
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from allbackend.serializers import UserSerializer, GroupSerializer, TeamSerializer, PlayerSerializer
from allbackend.permissions import IsMeOrReadOnlyAdmin, UserListCreatePermissions
from .models import Team, Player
from django.contrib.auth import get_user_model

User = get_user_model()


class UsersViewSet(generics.ListCreateAPIView):
    permission_classes = (UserListCreatePermissions,)
    queryset = User.objects.all().order_by('last_name', 'first_name', 'username')
    serializer_class = UserSerializer


class GroupViewSet(generics.ListCreateAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]


class TeamViewSet(generics.ListCreateAPIView):
    queryset = Team.objects.all().order_by('name')
    serializer_class = TeamSerializer


class PlayerViewSet(generics.ListCreateAPIView):
    queryset = Player.objects.all().order_by('nickname')
    serializer_class = PlayerSerializer


class CurrentUserView(APIView):
    def get(self, request):

        serializer = UserSerializer(request.user, context={'request': request})
        return Response(serializer.data)


class UserCreateAPIView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
