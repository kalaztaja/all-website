from rest_framework import permissions


class IsMeOrReadOnlyAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if (request.method in permissions.SAFE_METHODS
                and request.user and request.user.is_staff):
            return True
        return obj == request.user


class UserListCreatePermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        return request.user and request.user.is_staff
