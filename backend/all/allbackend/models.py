from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

import uuid


class User(AbstractUser):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)


class Team(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=40, blank=False,
                            default='', unique=True)
    tag = models.TextField(blank=False, unique=True)


class Player(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, default="")
    nickname = models.CharField(max_length=40, blank=False)
    team = models.OneToOneField(Team,
                                on_delete=models.PROTECT,
                                primary_key=True,
                                default=None,
                                db_constraint=False)
