from django.apps import AppConfig


class AllbackendConfig(AppConfig):
    name = 'allbackend'
